import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../service/api.service';

@Component({
  selector: 'app-race-list',
  templateUrl: './race-list.component.html',
  styleUrls: ['./race-list.component.css']
})

export class RaceListComponent implements OnInit {
  
  Race:any = [];

  constructor(private apiService: ApiService) { 
    this.readRace();
  }

  ngOnInit() {}

  readRace(){
    this.apiService.getRaces().subscribe((data) => {
     this.Race = data;
    })    
  }

  removeRace(race, index) {
    if(window.confirm('Are you sure?')) {
        this.apiService.deleteRace(race._id).subscribe((data) => {
          this.Race.splice(index, 1);
        }
      )    
    }
  }

}