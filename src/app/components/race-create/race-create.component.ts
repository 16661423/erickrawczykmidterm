import { Router } from '@angular/router';
import { ApiService } from './../../service/api.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-race-create',
  templateUrl: './race-create.component.html',
  styleUrls: ['./race-create.component.css']
})

export class RaceCreateComponent implements OnInit {  
  submitted = false;
  raceForm: FormGroup;
  RaceProfile:any = ['Tiny', 'Small', 'Medium', 'Large', 'Huge', 'Gargantuan']
  
  constructor(
    public fb: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private apiService: ApiService
  ) { 
    this.mainForm();
  }

  ngOnInit() { }

  mainForm() {
    this.raceForm = this.fb.group({
      name: ['', [Validators.required]],
      size: ['', [Validators.required]],
      speed: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      languages: [''],
      strengthAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      dexterityAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      constitutionAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      intelligenceAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      wisdomAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      charismaAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      abilities: ['', [Validators.required]]
      //email: ['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
      //designation: ['', [Validators.required]],
      //phoneNumber: ['', [Validators.required, Validators.pattern('^[0-9]+$')]]
    })
  }

  // Choose size with select dropdown
  updateProfile(e){
    this.raceForm.get('size').setValue(e, {
      onlySelf: true
    })
  }

  // Getter to access form control
  get myForm(){
    return this.raceForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (!this.raceForm.valid) {
      return false;
    } else {
      this.apiService.createRace(this.raceForm.value).subscribe(
        (res) => {
          console.log('Race successfully created!')
          this.ngZone.run(() => this.router.navigateByUrl('/races-list'))
        }, (error) => {
          console.log(error);
        });
    }
  }
}