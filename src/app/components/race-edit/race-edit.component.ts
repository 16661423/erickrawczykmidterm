import { Race } from './../../model/Race';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { ApiService } from './../../service/api.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";


@Component({
  selector: 'app-race-edit',
  templateUrl: './race-edit.component.html',
  styleUrls: ['./race-edit.component.css']
})

export class RaceEditComponent implements OnInit {
  submitted = false;
  editForm: FormGroup;
  raceData: Race[];
  RaceProfile:any = ['Tiny', 'Small', 'Medium', 'Large', 'Huge', 'Gargantuan']

  constructor(
    public fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private apiService: ApiService,
    private router: Router
  ) {}

  ngOnInit() {
    this.updateRace();
    let id = this.actRoute.snapshot.paramMap.get('id');
    this.getRace(id);
    this.editForm = this.fb.group({
      name: ['', [Validators.required]],
      size: ['', [Validators.required]],
      speed: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      languages: ['', [Validators.required]],
      strengthAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      dexterityAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      constitutionAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      intelligenceAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      wisdomAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      charismaAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      abilities: ['', [Validators.required]]
    })
  }

  // Choose options with select-dropdown
  updateProfile(e) {
    this.editForm.get('designation').setValue(e, {
      onlySelf: true
    })
  }

  // Getter to access form control
  get myForm() {
    return this.editForm.controls;
  }

  getRace(id) {
    this.apiService.getRace(id).subscribe(data => {
      this.editForm.setValue({
        name: data['name'],
        size: data['size'],
        speed: data['speed'],
        languages: data['languages'],
        strengthAdd: data['strengthAdd'],
        dexterityAdd: data['dexterityAdd'],
        constitutionAdd: data['constitutionAdd'],
        intelligenceAdd: data['intelligenceAdd'],
        wisdomAdd: data['wisdomAdd'],
        charismaAdd: data['charismaAdd'],
        abilities: data['abilities']
      });
    });
  }

  updateRace() {
    this.editForm = this.fb.group({
      name: ['', [Validators.required]],
      size: ['', [Validators.required]],
      speed: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      languages: ['', [Validators.required]],
      strengthAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      dexterityAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      constitutionAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      intelligenceAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      wisdomAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      charismaAdd: ['', [Validators.pattern('^-?[0-9]+$')]],
      abilities: ['', [Validators.required]]
    })
  }

  onSubmit() {
    this.submitted = true;
    if (!this.editForm.valid) {
      return false;
    } else {
      if (window.confirm('Are you sure?')) {
        let id = this.actRoute.snapshot.paramMap.get('id');
        this.apiService.updateRace(id, this.editForm.value)
          .subscribe(res => {
            this.router.navigateByUrl('/races-list');
            console.log('Content updated successfully!')
          }, (error) => {
            console.log(error)
          })
      }
    }
  }

}