import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RaceCreateComponent } from './components/race-create/race-create.component';
import { RaceListComponent } from './components/race-list/race-list.component';
import { RaceEditComponent } from './components/race-edit/race-edit.component';
import { HomepageComponent } from './components/homepage/homepage.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'create-race' },
  { path: 'create-race', component: RaceCreateComponent },
  { path: 'edit-race/:id', component: RaceEditComponent },
  { path: 'races-list', component: RaceListComponent },
  { path: 'homepage', component: HomepageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }


