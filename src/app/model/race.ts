export class Race {
    name: string;
    size: string;
    speed: number;
    languages: string;
    strengthAdd: number;
    dexterityAdd: number;
    constitutionAdd: number;
    intelligenceAdd: number;
    wisdomAdd: number;
    charismaAdd: number;
    abilities: string;
}