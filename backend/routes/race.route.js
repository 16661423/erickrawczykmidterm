const express = require('express');
const app = express();
const raceRoute = express.Router();

// Race model
let Race = require('../models/Race');

// Add Race
raceRoute.route('/create').post((req, res, next) => {
  Race.create(req.body, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
});

// Get All Races
raceRoute.route('/').get((req, res) => {
  Race.find((error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})

// Get single race
raceRoute.route('/read/:id').get((req, res) => {
  Race.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error)
    } else {
      res.json(data)
    }
  })
})


// Update race
raceRoute.route('/update/:id').put((req, res, next) => {
  Race.findByIdAndUpdate(req.params.id, {
    $set: req.body
  }, (error, data) => {
    if (error) {
      return next(error);
      console.log(error)
    } else {
      res.json(data)
      console.log('Data updated successfully')
    }
  })
})

// Delete race
raceRoute.route('/delete/:id').delete((req, res, next) => {
  Race.findOneAndRemove(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      })
    }
  })
})

module.exports = raceRoute;