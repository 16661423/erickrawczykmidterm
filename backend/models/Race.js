const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema
let Race = new Schema({
   name: {
      type: String
   },
   size: {
    type: String
   },
   speed: {
    type: Number
   },
   languages: {
    type: String
   },
   strengthAdd: {
    type: Number
   },
   dexterityAdd: {
    type: Number
   },
   constitutionAdd: {
    type: Number
   },
   intelligenceAdd: {
    type: Number
   },
   wisdomAdd: {
    type: Number
   },
   charismaAdd: {
    type: Number
   },
   abilities: {
    type: String
   }
}, {
   collection: 'races'
})

module.exports = mongoose.model('Race', Race)